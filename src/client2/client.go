package main

import (
	"context"
	"log"
	"os"
	"time"

	//"github.com/golang/protobuf/proto"
	// "github.com/golang/example/stringutil"
	//pb "google.golang.org/grpc/main"
	"../message"
	"google.golang.org/grpc"
)

const (
	address     = "localhost:50051"
	defaultName = "Kuzmanovic"
)

func main() {
	// Set up a connection to the server.
	conn, err := grpc.Dial(address, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()
	c := message.NewPersonServiceClient(conn)

	// Contact the server and print out its response.
	name := defaultName
	if len(os.Args) > 1 {
		name = os.Args[1]
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	var phone = &message.Person_PhoneNumber	{
		Number: "213123123",
		Type: 0, 
	}
	r, err := c.Vrati(ctx, 
				&message.Person{
					Name: name,
					Age: 50,
					Email: "sa@na.ca",
					Phone: phone,

			})
	if err != nil {
		log.Fatalf("could not greet: %v", err)
	}
	log.Printf("Greeting: %s %v %s %s", r.Name, r.Age, r.Email, r.Phone)
}
