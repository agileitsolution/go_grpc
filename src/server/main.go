package main

import (
	"context"
	"fmt"
	"log"
	"net"

	"github.com/golang/protobuf/proto"
	// "github.com/golang/example/stringutil"
	//pb "google.golang.org/grpc/main"

	"../message"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

const (
	port = ":50051"
)

// server is used to implement helloworld.GreeterServer.
type Echoserver struct{}

func (s *Echoserver) Echo(ctx context.Context, in *message.StringMessage) (*message.StringMessage, error) {
	log.Printf("Received: %v", in.Value)
	return &message.StringMessage{Value: "Hello " + in.Value}, nil
}

func main() {
	// fmt.Println(stringutil.Reverse())
	fmt.Println("stringutil.Reverse")
	msg := &message.StringMessage{
		Value: "This is the value"}

	data, err := proto.Marshal(msg)
	if err != nil {
		log.Fatal("marshaling error: ", err)
	}
	// printing out our raw protobuf object
	fmt.Println(data)

	// var type = &message.Person_PhoneType
	// {
	// 	name: &message.Person_PhoneType;
	// 	value: 
	// }
	var phone = &message.Person_PhoneNumber	{
		Number: "213123123",
		Type: 1, 
	}
	elliot := &message.Person{
		Name: "Elliot",
		Age:  24,
		Email: "elite@mail.com",
		Phone: phone,
	}

	data2, err := proto.Marshal(elliot)
	if err != nil {
		log.Fatal("marshaling error: ", err)
	}

	// printing out our raw protobuf object
	fmt.Println(data2)

	// let's go the other way and unmarshal
	// our byte array into an object we can modify
	// and use
	newElliot := &message.Person{}
	err = proto.Unmarshal(data2, newElliot)
	if err != nil {
		log.Fatal("unmarshaling error: ", err)
	}

	// print out our `newElliot` object
	// for good measure
	fmt.Println(newElliot.GetAge())
	fmt.Println(newElliot.GetName())
	fmt.Println(newElliot.GetEmail())
	fmt.Println(newElliot.GetPhone())

	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	s := grpc.NewServer()
	message.RegisterYourServiceServer(s, &Echoserver{})
	// Register reflection service on gRPC server.
	reflection.Register(s)
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
