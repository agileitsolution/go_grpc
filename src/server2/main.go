package main

import (
	"context"
	"fmt"
	"log"
	"net"

	"github.com/golang/protobuf/proto"
	// "github.com/golang/example/stringutil"
	//pb "google.golang.org/grpc/main"

	"../message"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

const (
	port = ":50051"
)

// server is used to implement helloworld.GreeterServer.
type Vratiserver struct{}




func (s *Vratiserver) Vrati(ctx context.Context, in *message.Person) (*message.Person, error) {
	log.Printf("Received: %s %v %s %s", in.GetAge, in.GetName, in.GetEmail, in.Phone)
	// return &message.Person{Value: "Hello " + in.Value}, nil
	return &message.Person{
		 Name:  "Sladjan "+in.Name, 
		 Age: in.Age,
		 Email: in.Email,
		 Phone: in.Phone, 
		}, nil
}

func main() {
	var phone = &message.Person_PhoneNumber	{
		Number: "213123123",
		Type: 0, 
	}
	elliot := &message.Person{
		Name: "Elliot",
		Age:  24,
		Email: "elite@mail.com",
		Phone: phone,
	}
	data2, err := proto.Marshal(elliot)
	if err != nil {
		log.Fatal("marshaling error: ", err)
	}

	// printing out our raw protobuf object
	fmt.Println(data2)

	// let's go the other way and unmarshal
	// our byte array into an object we can modify
	// and use
	newElliot := &message.Person{}
	err = proto.Unmarshal(data2, newElliot)
	if err != nil {
		log.Fatal("unmarshaling error: ", err)
	}

	// print out our `newElliot` object
	// for good measure
	fmt.Println(newElliot.GetAge())
	fmt.Println(newElliot.GetName())
	fmt.Println(newElliot.GetEmail())
	fmt.Println(newElliot.GetPhone())

	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	s := grpc.NewServer()
	message.RegisterPersonServiceServer(s, &Vratiserver{})
	// Register reflection service on gRPC server.
	reflection.Register(s)
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
